class CrawlerWidget {
  constructor(params) {
    if (!params || !params.selector) {
      throw new Error('CrawlerWidget requires a selector parameter');
    }
    this.context = this.context || document;
    Object.assign(this, params);
  }

  get isUserFriendly() {
    // Changed to select the element each time
    this.element = this.context.isUserFriendly(this.selector);
    return this.element;
    // this.element = this.element || this.context.isUserFriendly(this.selector);
    // return this.element;
  }
}

class ReadableWidget extends CrawlerWidget {
  constructor(params) {
    if (params && !params.parser) {
      params.parser = Parsers.innerText; //default parser
    }
    super(params);
  }

  get value() {
    if (this.isUserFriendly) {
      return this.parser(this.element, this.options);
    } else {
      shared.devlog(`ReadableWidget (selector: '${this.selector}') cannot be read with the assigned parser`);
      return '';
    }
  }
}

let persistence, shared;

function createShared() {
  let flowControl;
  let config = {};
  function initializeConfig() {
    // Defaults:
    config['devlog.enabled'] = false;
    config['devlog.maxLines'] = 200;
    config['defaults.extraInterval'] = true;
    config['defaults.timeout'] = 4;
    config['defaults.postponeMinutes'] = 65; //0: Random between min and max
    config['defaults.postponeMinutes.min'] = 65;
    config['defaults.postponeMinutes.max'] = 65;
    config['defaults.workInBackground'] = true;
    config['defaults.nextRun.useCountdown'] = true;
    config['defaults.nextRun'] = 60; //0: Random between min and max
    config['defaults.nextRun.min'] = 60;
    config['defaults.nextRun.max'] = 60;
    config['defaults.sleepMode'] = false;
    config['defaults.sleepMode.min'] = "00:00";
    config['defaults.sleepMode.max'] = "01:00";
    config['cf.autologin'] = false;
    config['cf.credentials.mode'] = 1;
    config['cf.credentials.email'] = 'YOUR@EMAIL.com';
    config['cf.credentials.password'] = 'YOURPASSWORD';
    config['cf.sleepHoursIfIpBan'] = 8;
    // config['fb.activateRPBonus'] = true;
    // config['fp.hoursBetweenRuns'] = 6;
    config['fp.maxTimeInMinutes'] = 15;
    config['bk.withdrawMode'] = "0";
    config['bk.hoursBetweenWithdraws'] = 4;
    config['bk.sleepMinutesIfIpBan'] = 75;
    config['bestchange.address'] = '101';
    config['ui.runtime'] = 0;
    config['fpb.credentials.mode'] = 2;
    config['fpb.credentials.username'] = 'YOUR_USERNAME';
    config['fpb.credentials.password'] = 'YOURPASSWORD';
    config['bigbtc.postponeMinutes'] = '0';
    config['fbch.credentials.mode'] = 2;
    config['fbch.credentials.username'] = 'YOUR_USERNAME';
    config['fbch.credentials.password'] = 'YOURPASSWORD';
    config['jtfey.credentials.mode'] = 2;
    config['jtfey.credentials.username'] = 'YOUR_USERNAME';
    config['jtfey.credentials.password'] = 'YOURPASSWORD';

    let storedData = persistence.load('config', true);
    if (storedData) {
      for (const prop in config) {
        if (storedData.hasOwnProperty(prop)) {
          config[prop] = storedData[prop];
        }
      }
    }

    config.version = GM_info.script.version;
    // console.log('VERSION:', config.version);
  };
  function getConfig() {
    return config;
  };
  function updateConfig(items) {
    items.forEach(function (item) {
      config[item.prop] = item.value;
    });
    persistence.save('config', config, true);
  };
  function devlog(msg, elapsed = false, reset = false) {
    if (!config['devlog.enabled']) {
      return;
    }

    let log;
    if (reset) {
      log = [`${helpers.getPrintableTime()}|Log cleared`];
    } else {
      log = persistence.load('devlog', true);
      log = log ?? [];
    }

    if (msg) {
      let previous;
      try {
        previous = log[log.length - 1].split('|')[1];
      } catch { }
      if (elapsed && (previous == msg)) {
        log[log.length - 1] = `${helpers.getPrintableTime()}|${msg}|[Elapsed time: ${elapsed} seconds]`;
      } else {
        log.push(`${helpers.getPrintableTime()}|${msg}`);
      }
    }

    if (log.length > 200) {
      log.splice(0, log.length - 200);
    }

    persistence.save('devlog', log, true);
  };
  function getDevLog() {
    let log;
    log = persistence.load('devlog', true);
    if (log) {
      return log;
    }
  };
  function isOpenedByManager() {
    loadFlowControl();
    if (!flowControl) {
      return false;
    }

    shared.devlog(`Visit to: ${flowControl.url}`);
    if (flowControl.type == K.WebType.CBG) {
      if (window.location.href.includes(flowControl.url) || window.location.href.includes(flowControl.host)) {
        shared.devlog(`Visit [CBG] returning true`);
        return true;
      } else {
        shared.devlog(`Visit [CBG] returning false`);
        return false;
      }
      // // Ignore if full domain
      // if(flowControl.host == window.location.host) {
      //     return false;
      // }
    } else if (flowControl.host != window.location.host) {
      return false;
    }

    if (flowControl.opened && flowControl.type != K.WebType.FAUCETPAY && flowControl.type != K.WebType.BAGIKERAN) {
      return false;
    }
    if (flowControl.type == K.WebType.BAGIKERAN && !window.location.href.includes(flowControl.params.trackUrl)) {
      return false;
    }

    return true;
  };
  function setFlowControl(id, url, webType, params = null) {
    flowControl = {
      id: id,
      changedAt: Date.now(),
      url: url,
      host: url.host,
      type: webType,
      opened: false,
      error: false,
      result: {}
    };
    if (params) {
      flowControl.params = params;
    }
    saveFlowControl();
  };
  function wasVisited(expectedId) {
    loadFlowControl();
    return flowControl.id == expectedId && flowControl.opened;
  };
  function hasErrors(expectedId) {
    return flowControl.id == expectedId && flowControl.error;
  };
  function getResult() {
    return flowControl.result;
  };
  function getCurrent() {
    return flowControl;
  };
  function saveAndclose(runDetails, delay = 0) {
    markAsVisited(runDetails);
    shared.devlog(`${window.location.href} closing`);
    if (delay) {
      setTimeout(window.close, delay);
    } else {
      setTimeout(window.close, 1000);
    }
  };
  function loadFlowControl() {
    flowControl = persistence.load('flowControl', true);
  };
  function saveFlowControl() {
    persistence.save('flowControl', flowControl, true);
  };
  function markAsVisited(runDetails) {
    flowControl.opened = true;
    flowControl.result = runDetails;
    saveFlowControl();
  };
  function addError(errorType, errorMessage) {
    flowControl.error = true;

    flowControl.result.errorType = errorType;
    flowControl.result.errorMessage = errorMessage;

    saveFlowControl();
  };
  function closeWithError(errorType, errorMessage) {
    addError(errorType, errorMessage);
    shared.devlog(`${window.location.href} closing with error msg`);
    window.close();
  };
  function clearFlowControl() {
    flowControl = {};
    saveFlowControl();
  };
  function clearRetries() {
    loadFlowControl();
    flowControl.retrying = false;
    saveFlowControl();
    return false;
  };
  function isRetrying() {
    if (flowControl.retrying) {
      return true;
    }
    flowControl.retrying = true;
    saveFlowControl();
    return false;
  };
  function setProp(key, val) {
    flowControl[key] = val;
    saveFlowControl();
  };
  function getProp(key) {
    return flowControl[key];
  };
  initializeConfig();
  return {
    devlog: devlog,
    getDevLog: getDevLog,
    setFlowControl: setFlowControl,
    wasVisited: wasVisited,
    isOpenedByManager: isOpenedByManager,
    saveFlowControl: saveFlowControl,
    getCurrent: getCurrent,
    getResult: getResult,
    addError: addError,
    closeWindow: saveAndclose,
    closeWithError: closeWithError,
    updateWithoutClosing: markAsVisited,
    hasErrors: hasErrors,
    clearFlowControl: clearFlowControl,
    getConfig: getConfig,
    updateConfig: updateConfig,
    clearRetries: clearRetries,
    isRetrying: isRetrying,
    setProp: setProp,
    getProp: getProp
  };
};

function createPersistence() {
  const prefix = 'common_';
  function save(key, value, parseIt = false) {
    GM_setValue(prefix + key, parseIt ? JSON.stringify(value) : value);
  };
  function load(key, parseIt = false) {
    let value = GM_getValue(prefix + key);
    if (value && parseIt) {
      value = JSON.parse(value);
    }
    return value;
  };
  return {
    save: save,
    load: load
  };
};

persistence = createPersistence();
shared = createShared();

console.log('common loaded');
